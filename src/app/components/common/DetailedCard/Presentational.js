import React from 'react';
import { 
  Button, 
  Card, 
  Avatar,
  Row,
  Col,
} from 'antd';
import PropTypes from 'prop-types';
import i18n from 'i18next';

import classes from './Styles.module.scss';

const Presentational = ({
  id,
  avatar, 
  login,
  url,
}) => {
  const { Meta } = Card;

  return (
    <Card className={classes.Card}>
      <Meta
        className={classes.Meta}
        avatar={
          <Avatar className={classes.Avatar} size="large" src={avatar} />
        }
        title={`${i18n.t('userDetails.id')}: ${id}`}
        description={(
          <Row gutter={[0, 15]}>
            <Col span={24}>
              <span>
                {`${i18n.t('userDetails.login')}: ${login}`}
              </span>
            </Col>
            <Col span={24}>
              <span>
                {`${i18n.t('userDetails.url')}: ${url}`}
              </span>
            </Col>
            <Col span={24}>
              <Button 
                type="primary"
                onClick={() => window.location.replace(url)}
              >
                {i18n.t('userDetails.button')}
              </Button>
            </Col>
          </Row>
        )}
      />
    </Card>
  );
};

Presentational.propTypes = {
  id: PropTypes.number.isRequired,
  avatar: PropTypes.string.isRequired,
  login: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
}

export default Presentational;
