import React from 'react';
import { withRouter } from 'react-router-dom';
import { 
  Button, 
  Card, 
  Avatar, 
  Col, 
} from 'antd';
import PropTypes from 'prop-types';
import i18n from 'i18next';

import routes from '../../../constants/routes';
import classes from './Styles.module.scss';

const Presentational = ({
  avatar, 
  login,
  history,
}) => {
  const { Meta } = Card;

  const onDetailsClicked = () => {
    history.push(`${routes.APP.USERS}/${login}`);
  }

  return (
    <Col className={classes.CardColumn} xs={24} sm={12} lg={6}>
      <Card className={classes.Card}>
        <Meta
          className={classes.Meta}
          avatar={
            <Avatar src={avatar} />
          }
          title={login}
          description={(
            <Button type="primary" onClick={onDetailsClicked}>
              {i18n.t('users.button')}
            </Button>
          )}
        />
      </Card>
    </Col>
  );
};

Presentational.propTypes = {
  avatar: PropTypes.string.isRequired,
  login: PropTypes.string.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
}

export default React.memo(withRouter(Presentational));
