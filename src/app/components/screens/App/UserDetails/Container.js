import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import Presentational from './Presentational';

import { getUserDetails } from '../../../../redux/modules/users/actions/readOne';
import { clearState } from '../../../../utils/actions';

const mapStateToProps = (state) => ({
  userDetails: state.users._data 
    ? state.users._data.userDetails 
    : null,
  isReadingUser: state.users.isReading,
  error: state.users.error,
});

const mapDispatchToProps = (dispatch) => ({
  getUserDetails: (params) => dispatch(getUserDetails(params)),
  clearState: () => clearState(dispatch, 'users'),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Presentational),
);
