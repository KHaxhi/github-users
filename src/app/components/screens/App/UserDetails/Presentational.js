import React, { useEffect, useRef } from 'react';
import { 
  Row, 
  Col, 
  PageHeader, 
  Layout, 
  Spin,
  notification,
} from 'antd';
import i18n from 'i18next';
import PropTypes from 'prop-types';

import DetailedCard from '../../../common/DetailedCard/Presentational';
import classes from './Styles.module.scss';

const Presentational = ({
  userDetails,
  isReadingUser,
  error,
  getUserDetails,
  clearState,
  match,
  history,
}) => {
  const isFirstRender = useRef(true);
  const { Content } = Layout;

  useEffect(() => {
    const params = {
      username: match.params.login,
    }
  
    getUserDetails(params);
  }, [getUserDetails, match]);

  useEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false;
    } else if (error) {
      notification.error({
        message: i18n.t('global.error'),
        description: error,
      });
      clearState();
    }
  }, [error, clearState]);

  let userDetailsCard = (
    <div className={classes.Spinner}>
      <Spin size="large" spinning={isReadingUser} />
    </div>
  );

  if (userDetails && !isReadingUser) {
    userDetailsCard = (
      <DetailedCard
        id={userDetails.id}
        avatar={userDetails.avatar_url}
        login={userDetails.login}
        url={userDetails.html_url}
      />
    );
  }

  return (
    <Layout className={classes.Layout}>
      <PageHeader
        className={classes.Header}
        title={i18n.t('userDetails.title')}
        onBack={() => history.goBack()}
      />
      <Content className={classes.Content}>
        <Row
          justify="center"
          align="middle"
          className={classes.UserDetailsRow}
        >
          <Col>
            {userDetailsCard}
          </Col>
        </Row>
      </Content>
    </Layout>
  )
};

Presentational.propTypes = {
  userDetails: PropTypes.shape({
    id: PropTypes.number.isRequired,
    avatar_url: PropTypes.string.isRequired,
    login: PropTypes.string.isRequired,
    html_url: PropTypes.string.isRequired,
  }),
  isReadingUser: PropTypes.bool,
  error: PropTypes.string,
  getUserDetails: PropTypes.func.isRequired,
  clearState: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      login: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
}

Presentational.defaultProps = {
  userDetails: null,
  isReadingUser: false,
  error: null,
}

export default Presentational;
