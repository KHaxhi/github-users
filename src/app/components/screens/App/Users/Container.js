import { connect } from 'react-redux';

import Presentational from './Presentational';

import { getUsers } from '../../../../redux/modules/users/actions/readAll';
import { clearState } from '../../../../utils/actions';

const mapStateToProps = (state) => ({
  users: state.users._data 
    ? state.users._data.users 
    : null,
  isReadingUsers: state.users.isReading,
  error: state.users.error,
});

const mapDispatchToProps = (dispatch) => ({
  getUsers: (params) => dispatch(getUsers(params)),
  clearState: () => clearState(dispatch, 'users'),
});

export default connect(mapStateToProps, mapDispatchToProps)(Presentational);
