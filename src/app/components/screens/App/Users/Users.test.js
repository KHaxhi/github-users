import React from 'react';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router';
import { I18nextProvider } from 'react-i18next';
import i18n from 'i18next';
import {
  // Row,
  // Col,
  // Pagination,
  // PageHeader,
  // Layout,
  Spin,
  // notification,
} from 'antd';

import Presentational from './Presentational';
import UserCard from '../../../common/UserCard/Presentational';

describe('Test "Users" screen', () => {
  let wrapper;
  const getUsers = jest.fn();
  const clearState = jest.fn();
  const defaultMockProps = {
    users: null,
    isReadingUsers: false,
    error: null,
    getUsers,
    clearState,
  };

  beforeEach(() => {
    wrapper = mount(
      <MemoryRouter>
        <I18nextProvider i18n={i18n}>
          <Presentational {...defaultMockProps} />
        </I18nextProvider>
      </MemoryRouter>,
    );
  });

  it('should render the Spinner while reading users', () => {
    const mockProps = {
      ...defaultMockProps,
      isReadingUsers: true,
    };

    wrapper.setProps({
      children: <Presentational {...mockProps} />,
    });
    wrapper.update();

    expect(wrapper.find(Spin).props()).toHaveProperty('spinning', true);
  });

  it('should remove spinner and render user cards when users not empty', () => {
    const mockProps = {
      ...defaultMockProps,
      users: [
        {
          id: 1,
          avatar_url: 'test',
          login: 'test',
        },
      ],
      isReadingUsers: false,
    };

    wrapper.setProps({
      children: <Presentational {...mockProps} />,
    });
    wrapper.update();
    
    expect(wrapper.exists(Spin)).toBe(false);
    expect(wrapper.find(UserCard)).toHaveLength(1);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });
});
