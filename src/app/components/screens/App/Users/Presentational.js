import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { 
  Row, 
  Col, 
  PageHeader, 
  Layout,
  Button, 
  Spin,
  notification,
} from 'antd';

import i18n from 'i18next';

import UserCard from '../../../common/UserCard/Presentational';
import classes from './Styles.module.scss';

const onLoadMore = (users, getUsers) => {
  if (users) {
    const lastId = users[users.length - 1].id;
    const params = {
      perPage: 12,
      since: lastId,
    }
  
    getUsers(params);
  }
}

const Presentational = ({
  users,
  isReadingUsers,
  error,
  getUsers,
  clearState,
}) => {
  const isFirstRender = useRef(true);
  const [usersCount, setUsersCount] = useState();
  const { Content } = Layout;

  useEffect(() => {
    if (!users) {
      const params = {
        perPage: 12,
        since: 1,
      }
    
      getUsers(params);
    }
  }, [getUsers, users]);

  useEffect(() => {
    if (users) setUsersCount(users.length);
  }, [users]);

  useEffect(() => {
    if (isFirstRender.current) {
      isFirstRender.current = false;
    } else if (error) {
      notification.error({
        message: i18n.t('global.error'),
        description: error,
      });
      clearState();
    }
  }, [error, clearState]);

  let usersContent = (
    <div className={classes.Spinner}>
      <Spin size="large" spinning />
    </div>
  );

  if (users) {
    usersContent = users.map((user) => ( 
      <UserCard 
        key={user.id}
        avatar={user.avatar_url}
        login={user.login}
      />
    ))
  }
  
  return (
    <Layout className={classes.Layout}>
      <PageHeader
        className={classes.Header}
        title={i18n.t('users.title')}
      />
      <Content>
        <Row gutter={[15, 15]} className={classes.Users}>
          {usersContent}
        </Row>
        <Row
          justify="center"
          className={classes.LoadMoreRow}
        >
          <Col xs={12} sm={10} md={8} lg={6}>
            <Button 
              type="ghost"
              size="large"
              loading={usersCount && isReadingUsers}
              className={classes.LoadMore}
              onClick={() => onLoadMore(
                users,
                getUsers,
              )}
            >
              {i18n.t('users.loadMore')}
            </Button>
          </Col>
        </Row>
      </Content>
    </Layout>
  );
};

Presentational.propTypes = {
  users: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    avatar_url: PropTypes.string.isRequired,
    login: PropTypes.string.isRequired,
  })),
  isReadingUsers: PropTypes.bool,
  error: PropTypes.string,
  getUsers: PropTypes.func.isRequired,
  clearState: PropTypes.func.isRequired,
}

Presentational.defaultProps = {
  users: null,
  isReadingUsers: false,
  error: null,
}

export default Presentational;
