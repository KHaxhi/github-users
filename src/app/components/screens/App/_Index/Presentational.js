import React, { Suspense } from 'react';
import { 
  Switch, 
  Route, 
  Redirect, 
} from 'react-router-dom';

import Users from '../Users/Container';
import UserDetails from '../UserDetails/Container';
import routes from '../../../../constants/routes';

const NotFound = React.lazy(() => import('../../404/Presentational'));

const Presentational = () => (
  <Switch>
    <Route 
      exact
      path={`${routes.APP.USERS}/:login`}
      component={UserDetails}
    />
    <Route
      exact
      path={routes.APP.USERS}
      component={Users}
    />
    <Route 
      exact 
      path={routes.APP.INDEX}
    >
      <Redirect to={routes.APP.USERS} />
    </Route>
    <Route
      path="*" 
      render={() => (
        <Suspense fallback={<></>}>
          <NotFound />
        </Suspense>
      )} 
    />
  </Switch>
);

export default Presentational;
