const APP_INDEX = '/app';

const routes = {
  APP: {
    INDEX: APP_INDEX,
    USERS: `${APP_INDEX}/users`,
  },
};

export default routes;
