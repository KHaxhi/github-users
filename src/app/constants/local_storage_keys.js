const prefix = 'github-users';

export default {
  SELECTED_LANGUAGE: `${prefix}-selected-language`,
};
