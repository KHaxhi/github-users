import reducer from './reducers';
import types from './action_types';
import status from '../../../constants/action_status';
import { initialState } from '../../../utils/reducers';

describe('Test users reducer', () => {
  const usersArray = [
    {
      id: 1,
      avatar_url: 'test',
      login: 'test',
    },
  ];
  let state;

  it('should return initial state', () => {
    expect(reducer(state, {})).toEqual(initialState);
  });

  it('should update state after reading users', () => {
    const startAction = {
      type: types.GET_USERS,
      status: status.ACTION_START,
    };

    expect(reducer(state, startAction)).toEqual({
      ...initialState,
      isReading: true,
      didRead: false,
    });

    const successAction = {
      type: types.GET_USERS,
      status: status.ACTION_SUCCESS,
      payload: {
        users: [...usersArray],
      },
    };

    state = reducer(state, successAction);

    expect(reducer(state, successAction)).toEqual({
      ...state,
      isReading: false,
      didRead: true,
      _data: state._data?.users
        ? { users: [...state._data.users, ...usersArray] }
        : { users: [...usersArray] },
    });
  });
});
