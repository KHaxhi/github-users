import { initialState, baseReducer } from '../../../utils/reducers';
import types from './action_types';
import actions from '../../../constants/action_type';

const initState = {
  ...initialState,
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case types.GET_USERS:
      return baseReducer(
        state,
        action,
        actions.READ,
        null,
        () => (state._data?.users
          ? { users: [...state._data.users, ...action.payload.users] } 
          : { users: [...action.payload.users] }
        ),
        null,
      );
    case types.GET_USER_DETAILS:
      return baseReducer(
        state,
        action,
        actions.READ,
        null,
        () => ({
          users: [
            ...state._data.users,
          ],
          userDetails: {
            ...action.payload.user,
          },
        }),
        null,
      );
    default:
      return state;
  }
};

export default reducer;
