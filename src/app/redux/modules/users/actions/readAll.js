import axios from 'axios';

import actionTypes from '../action_types';
import config from '../../../../config';
import getError from '../../../../utils/error_message';
import { start, success, fail } from '../../../../utils/actions';

/**
 * Get users
 *
 * @param {String} (header) accept: Accept header
 * @param {String} (query) since: ID from which to start listing
 * @param {String} (query) per_page: Results per page (max 100)
 */

export const getUsers = (params) => async (dispatch) => {
  try {
    start(dispatch, actionTypes.GET_USERS);

    const headers = {
      accept: 'application/vnd.github.v3+json',
    };

    const query = `?since=${params.since}&per_page=${params.perPage}`;
    const URL = `${config.SERVER_URL}/users${query}`;

    const response = await axios({
      method: 'get',
      url: URL,
      headers,
    });
    const { data } = response;

    if (response.statusText === 'OK') {
      success(dispatch, actionTypes.GET_USERS, {
        users: [...data],
      });
    }
  } catch (error) {
    fail(dispatch, actionTypes.GET_USERS, { error: getError(error) });
  }
};
