import axios from 'axios';

import actionTypes from '../action_types';
import config from '../../../../config';
import getError from '../../../../utils/error_message';
import { start, success, fail } from '../../../../utils/actions';

/**
 * Get users
 *
 * @param {String} (header) accept: Accept header
 * @param {String} (path) username: User login
 */

export const getUserDetails = (params) => async (dispatch) => {
  try {
    start(dispatch, actionTypes.GET_USER_DETAILS);

    const headers = {
      accept: 'application/vnd.github.v3+json',
    };

    const URL = `${config.SERVER_URL}/users/${params.username}`;

    const response = await axios({
      method: 'get',
      url: URL,
      headers,
    });
    const { data } = response;

    if (response.statusText === 'OK') {
      success(dispatch, actionTypes.GET_USER_DETAILS, {
        user: { ...data },
      });
    }
  } catch (error) {
    fail(dispatch, actionTypes.GET_USER_DETAILS, { error: getError(error) });
  }
};
