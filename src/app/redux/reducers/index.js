import { combineReducers } from 'redux';

import usersReducer from '../modules/users/reducers';

const appReducer = combineReducers({
  users: usersReducer,
});

const rootReducer = (state, action) => {
  if (action.type === 'CLEAR_GIVEN_STATE') {
    state = {
      ...state,
      [action.payload]: {
        ...state[action.payload],
        isCreating: false,
        didCreate: false,
        isFetching: false,
        didFetch: false,
        isUpdating: false,
        didUpdate: false,
        isDeleting: false,
        didDelete: false,
        error: '',
      },
    };
  }

  return appReducer(state, action);
};

export default rootReducer;
